import string


def generate_keyword(key_word, text_len):
    while len(key_word) < text_len:
        key_word = key_word + key_word

    temp_len = len(key_word)
    if temp_len > text_len:
        diff = temp_len - text_len
        key_word = key_word[:-diff]

    return key_word


def read_file():
    with open('input.txt', encoding='utf-8') as f:
        read_data = f.read()

    return read_data


def generate_alphabet():
    alphabet = list(string.ascii_letters)
    alphabet.extend(string.digits)
    alphabet.extend(string.punctuation)
    alphabet.extend(' ')

    russian_alphabet = ['а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с',
                        'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я']

    russian_upper = []

    alphabet.extend(russian_alphabet)

    for letter in russian_alphabet:
        russian_upper.append(letter.upper())

    alphabet.extend(russian_upper)

    return alphabet


def main():
    input_text = read_file()
    key_word = input("Enter keyword:")

    key_word = generate_keyword(key_word, len(input_text))

    alphabet = generate_alphabet()

    alphabet_len = len(alphabet)

    encryption_alphabet = {}
    decryption_alphabet = {}

    for i, val in enumerate(alphabet):
        encryption_alphabet[val] = i
        decryption_alphabet[i] = val

    print("Modified keyword:", key_word)
    print("Alphabet:", list(encryption_alphabet.keys()))

    encrypted_word = []

    for i in range(len(input_text)):
        source_letter = input_text[i]
        key_letter = key_word[i]
        source_value = encryption_alphabet[source_letter]
        key_value = encryption_alphabet[key_letter]

        enc_value = (source_value + key_value) % alphabet_len
        encrypted_word.append(decryption_alphabet[enc_value])

    print("Encrypted word:", "".join(encrypted_word))

    decrypted_word = []

    for i in range(len(encrypted_word)):
        encrypted_letter = encrypted_word[i]
        key_letter = key_word[i]
        encrypted_value = encryption_alphabet[encrypted_letter]
        key_value = encryption_alphabet[key_letter]

        dec_value = (encrypted_value - key_value) % alphabet_len
        decrypted_word.append(decryption_alphabet[dec_value])

    print("Decrypted word:", "".join(decrypted_word))


if __name__ == '__main__':
    main()
