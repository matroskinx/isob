class AuthServer:
    KAS_TGS = 1902  # AS и TGS пользуются этим общим ключом
    as_server_id = 9123

    def __init__(self):
        self.user_db = {"Vladislav": "my_key"}

    def request_auth(self, login, key):
        """
        login = "Vladislav" (c)
        key = "my_key"      (K_c)
        """
